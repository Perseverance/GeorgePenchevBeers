# Punk Beer Web App
*Objective: Use the punk beer api to create a unique project that uses templating to show data on the page. Use Angular.*

## Description
The [punk api](https://punkapi.com/) doesn’t require a key to use and provides a lot of information in its responses.

Create a web app listing of some/all beers. Possibly add serach if the API allows it. Allow your users to have a ‘shopping cart’ or ‘favourite’ functionality. You should then be able to show a separate list of the items you favourited. This will of course not be saved to any database.

![possible layout](https://cdn-images-1.medium.com/max/1200/1*z0dWsT-ud37k2lUbTM_8Hw.png)

## Some advice

This might seem like a giant leap at first, but focus on building this web app piece by piece. Create the app in the following order:

- **Do not use pure CSS. Set your angular to work with SCSS and use it!**

- Template data on the page from a generic API request
- Add a search bar, which makes a separate query to a database on request
- add favourite functionality which allows you to favourite a card
- create a separate route (using angular router) which allows you to see all the beers you’ve favourited in the session.