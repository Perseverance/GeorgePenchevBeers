import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import {NavBarComponent} from "./nav-bar/navbar.component";
import {BeersService} from "./services/beers.service";
import {AllBeersComponent} from "./beers/all-beers.component";
import {BeerThumbnailComponent} from "./beers/beer-thumbnail.component";
import {SearchBarComponent} from "./search-bar/search-bar.component";
import {HttpClientModule} from "@angular/common/http";
import {BeersListResolverService} from "./services/beers-list-resolver.service";
import {RouterModule} from "@angular/router";
import {appRoutes} from "./routes/routes";
import {FavoritesComponent} from "./beers/favorites.component";

@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    AllBeersComponent,
    BeerThumbnailComponent,
    SearchBarComponent,
    FavoritesComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    BeersService,
    BeersListResolverService],
  bootstrap: [AppComponent]
})
export class AppModule { }
