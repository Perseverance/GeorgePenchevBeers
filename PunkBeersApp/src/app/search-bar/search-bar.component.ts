import {Component} from "@angular/core";
import {BeersService} from "../services/beers.service";
import {Router} from "@angular/router";
@Component({
  selector: 'search-bar',
  templateUrl: 'search-bar.component.html',
  styleUrls: ['search-bar.component.scss']
})
export class SearchBarComponent {

  constructor(private beersService: BeersService, private router: Router) {

  }

  search(searchTerm) {
    console.log(searchTerm);
    this.beersService.searchForBeer(searchTerm).subscribe(results => {
      console.log(results)
      this.router.navigate(['/beers/search/', searchTerm]);
    })
  }

}
