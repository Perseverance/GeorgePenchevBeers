import {Component, Input, OnInit} from "@angular/core";
import {BeersService} from "../services/beers.service";
@Component({
  selector: 'beer-thumbnail',
  templateUrl: 'beer-thumbnail.component.html',
  styleUrls: ['beer-thumbnail.component.scss']
})
export class BeerThumbnailComponent implements OnInit {
  @Input() beer: any
  isFavorite: boolean;

  constructor(private beersService: BeersService) {

  }

  getDescription() {
    if (this.beer.description.length > 200) {
      return this.beer.description.substring(0, 200) + '...'
    } else {
      return this.beer.description
    }
  }

  addRemoveFavorites() {
    if (this.isFavorite) {
      this.beersService.removeFromFavorites(this.beer);
    } else {
      this.beersService.addToFavorites(this.beer);
    }
    this.isFavorite = !this.isFavorite;
  }

  ngOnInit() {
    this.isFavorite = this.beersService.isFavorite(this.beer);
  }
}
