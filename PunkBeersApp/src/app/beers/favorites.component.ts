import {Component, OnInit} from "@angular/core";
import {BeersService} from "../services/beers.service";
@Component({
  template: `
<div [routerLink]="['/favorites']" class="beer-container row">
  <div *ngFor="let beer of favoriteBeers" class="col-sm-4 nopadding">
    <beer-thumbnail [beer]="beer"></beer-thumbnail>
  </div>
</div>`
})

export class FavoritesComponent implements OnInit {
  favoriteBeers: any

  constructor(private beersService: BeersService) {

  }

  ngOnInit() {
    this.favoriteBeers = this.beersService.getFavorites();
  }
}
