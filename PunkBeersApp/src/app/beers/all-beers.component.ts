import {Component, OnInit} from "@angular/core";
import {BeersService} from "../services/beers.service";
import {ActivatedRoute, Router} from "@angular/router";
@Component({
  templateUrl: 'all-beers.component.html',
  styleUrls: ['all-beers.component.scss']
})
export class AllBeersComponent implements OnInit {

  beersToDisplay: any

  constructor(private beersService: BeersService, private route: ActivatedRoute, private router: Router) {
  }

  ngOnInit() {
      this.beersToDisplay = this.route.snapshot.data['allBeers'];
  }


  search(searchTerm) {
    this.beersService.searchForBeer(searchTerm).subscribe(results => {
      this.beersToDisplay = results;
    })
  }
}
