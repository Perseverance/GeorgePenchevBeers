import {AllBeersComponent} from "../beers/all-beers.component";
import {Routes} from "@angular/router";
import {BeersListResolverService} from "../services/beers-list-resolver.service";
import {FavoritesComponent} from "../beers/favorites.component";
export const appRoutes: Routes = [
  {path: '', redirectTo: '/beers', pathMatch: 'full'},
  {path: 'favorites', component: FavoritesComponent},
  {path: 'beers', component: AllBeersComponent, resolve: {allBeers: BeersListResolverService}, pathMatch: 'full'},
]
