import {Component} from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
 <div class="app container">
    <nav-bar class="row"></nav-bar>
    <router-outlet></router-outlet>
</div>
`,
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';
}
