import {Injectable, OnInit} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import 'rxjs/Rx';
import {forEach} from "@angular/router/src/utils/collection";
import {factoryOrValue} from "rxjs/operator/multicast";
@Injectable()
export class BeersService {

  allBeers: any = [];
  favoriteBeers: any = [];

  isDataAvailable: boolean = this.allBeers.length > 0;

  constructor(private httpClient: HttpClient) {
  }

  getBeers(): Observable<any[]> {
    if (this.allBeers.length > 0) {
      return this.allBeers;
    } else {
      return this.httpClient.get('https://api.punkapi.com/v2/beers')
        .map(result => {
            this.allBeers = result;
            return result;
          }
        )
    }
  }

  searchForBeer(searchTerm: string) {
    return this.httpClient.get('https://api.punkapi.com/v2/beers?beer_name=' + searchTerm)
  }

  getFavorites() {
    return this.favoriteBeers
  }

  isFavorite(beer: any) {
    return this.favoriteBeers.includes(beer);
  }

  addToFavorites(beer){
    this.favoriteBeers.push(beer);
    console.log(this.favoriteBeers)
  }

  removeFromFavorites(beer){
    this.favoriteBeers = this.favoriteBeers.filter(existingBeer => {
      return existingBeer.id !== beer.id;
    });
    console.log(this.favoriteBeers)
  }
}
