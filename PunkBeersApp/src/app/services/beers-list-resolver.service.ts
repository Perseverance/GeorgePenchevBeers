import {Injectable} from "@angular/core";
import {Resolve} from "@angular/router";
import {BeersService} from "./beers.service";
@Injectable()
export class BeersListResolverService implements Resolve<any> {

  constructor(private beersService: BeersService) {
  }

  resolve() {
    return this.beersService.getBeers().map(beers => beers);
  }
}
